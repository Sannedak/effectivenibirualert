package band.effective.effectivenibirualert.data.model.list

import com.squareup.moshi.Json

data class AsteroidsListResponse(

    @field:Json(name = "near_earth_objects")
    val days: Map<String, List<AsteroidListResponse>>
)

data class AsteroidListResponse(
    @field:Json(name = "id")
    val id: Int,
    @field:Json(name = "name")
    val name: String,
    @field:Json(name = "is_potentially_hazardous_asteroid")
    val isPotentiallyHazardous: Boolean,
    @field:Json(name = "estimated_diameter")
    val estimatedDiameter: DiameterResponse?,
    @field:Json(name = "close_approach_data")
    val approachData: List<ApproachDataResponse>
) {
    fun calculateAverageDiameter(): Double? {
        val diameterMeters = estimatedDiameter?.meters
        return (diameterMeters!!.max - diameterMeters.min) / 2
    }
}

data class DiameterResponse(
    @field:Json(name = "kilometers")
    val kilometers: DiameterMinMaxResponse?,
    @field:Json(name = "meters")
    val meters: DiameterMinMaxResponse?
)

data class DiameterMinMaxResponse(
    @field:Json(name = "estimated_diameter_min")
    val min: Double,
    @field:Json(name = "estimated_diameter_max")
    val max: Double
)

data class ApproachDataResponse(
    @field:Json(name = "close_approach_date")
    val arrivalDate: String,
    @field:Json(name = "relative_velocity")
    val relativeVelocity: RelativeVelocityResponse?,
    @field:Json(name = "miss_distance")
    val missDistance: MissDistanceResponse?,
    @field:Json(name = "orbiting_body")
    val orbitingBody: String
)

data class RelativeVelocityResponse(
    @field:Json(name = "kilometers_per_second")
    val kilometersPerSecond: Double,
    @field:Json(name = "kilometers_per_hour")
    val kilometersPerHour: Double
)

data class MissDistanceResponse(
    @field:Json(name = "astronomical")
    val astronomical: Double,
    @field:Json(name = "lunar")
    val lunar: Double,
    @field:Json(name = "kilometers")
    val kilometers: Double,
    @field:Json(name = "miles")
    val miles: Double
)