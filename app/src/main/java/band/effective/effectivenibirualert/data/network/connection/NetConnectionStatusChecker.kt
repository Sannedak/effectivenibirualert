package band.effective.effectivenibirualert.data.network.connection

interface NetConnectionStatusChecker {

    fun isActive(): Boolean
}