package band.effective.effectivenibirualert.data.network.exception

import band.effective.effectivenibirualert.data.network.connection.NetConnectionStatusChecker
import okhttp3.Interceptor
import okhttp3.Response

class NetExceptionsInterceptor(private val netChecker: NetConnectionStatusChecker) : Interceptor {

    private val restExceptionFactory = ServerExceptionFactory()
    private val badRequestParser = BadRequestParser()

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!netChecker.isActive()) throw Exceptions.NoInternet
        val response = chain.proceed(chain.request())
        if (response.code == 400) {
            val responseBody = response.body?.string() ?: "Empty Response Body"
            throw badRequestParser.parse(responseBody)
        }
        if (response.code > 400) throw restExceptionFactory.createFromResponse(response)
        return response
    }
}