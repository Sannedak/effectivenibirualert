package band.effective.effectivenibirualert.data.network

import android.app.Application
import band.effective.effectivenibirualert.data.network.connection.NetConnectionStatusCheckerImpl
import band.effective.effectivenibirualert.data.network.exception.NetExceptionsInterceptor
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit


object RetrofitFactory {

    private const val SOCKET_TIMEOUT_EXCEPTION_IN_SECONDS = 15L
    private const val BASE_URL = "https://api.nasa.gov"

    fun create(app: Application): Retrofit {
        return Retrofit.Builder()
            .client(createOkHttpClient(app))
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }

    private fun createOkHttpClient(app: Application): OkHttpClient {
        val interceptors = createInterceptors(app)
        return OkHttpClient.Builder()
            .connectTimeout(SOCKET_TIMEOUT_EXCEPTION_IN_SECONDS, TimeUnit.SECONDS)
            .readTimeout(SOCKET_TIMEOUT_EXCEPTION_IN_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(SOCKET_TIMEOUT_EXCEPTION_IN_SECONDS, TimeUnit.SECONDS)
            .apply {
                interceptors.forEach { addInterceptor(it) }
            }
            .build()
    }

    private fun createInterceptors(app: Application): List<Interceptor>{
        val netChecker = NetConnectionStatusCheckerImpl(app)
        return listOf(
            ApiKeyInterceptor(),
            NetExceptionsInterceptor(netChecker),
            createLoggingInterceptor()
        )
    }

    private fun createLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.HEADERS)
    }
}