package band.effective.effectivenibirualert.data.network.exception

import okhttp3.Response

class ServerExceptionFactory {

    fun createFromResponse(response: Response): Exception {
        return when (response.code) {
            422 -> Exceptions.UnprocessableEntity
            404 -> Exceptions.NotFound
            in 500 until 600 -> Exceptions.InternalServerError
            else -> Exceptions.UnknownServer
        }
    }
}