package band.effective.effectivenibirualert.data.repository.list

import band.effective.effectivenibirualert.data.model.list.AsteroidsListRequest
import band.effective.effectivenibirualert.data.model.list.AsteroidsListResponse
import band.effective.effectivenibirualert.data.network.exception.Exceptions
import band.effective.effectivenibirualert.di.NibiruApplication
import java.util.*

class AsteroidsListNasaNetRepository(private val service: AsteroidsListRetrofit) :
    AsteroidsListNetRepository {

    private val toNetDateFormatter = NibiruApplication().netDatePattern

    override suspend fun getAsteroidsList(startDate: Date, endDate: Date): AsteroidsListResponse {
        val request = createRequest(startDate, endDate)
        val response = service.getAsteroidsList(request.startDate, request.endDate)
        val shadowResponse = response.body()
        return shadowResponse ?: throw Exceptions.EmptyResponse
    }

    private fun createRequest(startDate: Date, endDate: Date) = AsteroidsListRequest(
        toNetDateFormatter.format(startDate),
        toNetDateFormatter.format(endDate)
    )
}