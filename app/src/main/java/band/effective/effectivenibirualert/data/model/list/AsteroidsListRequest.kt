package band.effective.effectivenibirualert.data.model.list

import com.squareup.moshi.Json

data class AsteroidsListRequest (

    @field:Json(name = "start_date")
    val startDate: String,
    @field:Json(name = "end_date")
    val endDate: String
)