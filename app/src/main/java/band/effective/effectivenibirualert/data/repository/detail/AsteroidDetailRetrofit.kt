package band.effective.effectivenibirualert.data.repository.detail

import band.effective.effectivenibirualert.data.model.detail.AsteroidDetailResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface AsteroidDetailRetrofit {
    @GET("/neo/rest/v1/neo/{asteroid_id}")
    suspend fun getAsteroidDetail(
        @Path("asteroid_id") asteroidId: String
    ): Response<AsteroidDetailResponse>
}