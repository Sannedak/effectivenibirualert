package band.effective.effectivenibirualert.data.model.favorites

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import band.effective.effectivenibirualert.presentation.fragments.detail.model.VisitsDetail

@Entity(tableName = "asteroid")
data class Asteroid(
    @PrimaryKey val id: Int,
    val name: String,
    val diameter: String,
    @ColumnInfo(name = "hazardous") val isPotentiallyHazardous: Boolean,
    @ColumnInfo(name = "approach_date") val approachDate: String,
    @ColumnInfo(name = "relative_velocity") val relativeVelocity: String,
    @ColumnInfo(name = "miss_distance") val missDistance: String,
    @ColumnInfo(name = "image_size") val imageSizePx: Int,
    @ColumnInfo(name = "orbit_type") val classType: String?,
    @ColumnInfo(name = "orbit_description") val classDescription: String?
)

@Entity(tableName = "visits")
data class Visit(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "id_in_list") val idList: Int,
    @ColumnInfo(name = "asteroid_id") val asteroidId: Int,
    @ColumnInfo(name = "approach_date") val approachDate: String,
    @ColumnInfo(name = "velocity") val relativeVelocity: String,
    @ColumnInfo(name = "miss_distance") val missDistance: String,
    @ColumnInfo(name = "orbiting_body") val orbitingBody: String
)

data class AsteroidWithVisits(
    @Embedded val asteroid: Asteroid,
    @Relation(parentColumn = "id", entityColumn = "asteroid_id")
    val visits: List<Visit>
)