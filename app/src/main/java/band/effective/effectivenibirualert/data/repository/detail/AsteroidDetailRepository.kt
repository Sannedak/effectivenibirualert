package band.effective.effectivenibirualert.data.repository.detail

import band.effective.effectivenibirualert.data.model.detail.AsteroidDetailResponse
import band.effective.effectivenibirualert.data.model.favorites.AsteroidWithVisits
import band.effective.effectivenibirualert.data.network.exception.Exceptions
import band.effective.effectivenibirualert.data.repository.favorites.AsteroidsFavoritesDao
import band.effective.effectivenibirualert.presentation.fragments.detail.model.UiAsteroidDetail
import band.effective.effectivenibirualert.presentation.fragments.detail.model.UiAsteroidDetailMapper
import band.effective.effectivenibirualert.presentation.fragments.detail.model.database.UiAsteroidDetailMapperDb
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async

class AsteroidDetailRepository(
    private val service: AsteroidDetailRetrofit,
    private val mapperNet: UiAsteroidDetailMapper,
    private val mapperDb: UiAsteroidDetailMapperDb,
    private val databaseDao: AsteroidsFavoritesDao
) : AsteroidDetailNetRepository {

    override suspend fun getAsteroidDetail(asteroidId: Int): UiAsteroidDetail {
        return if (checkAsteroidInDb(asteroidId)) {
            mapperDb.map(getFromDb(asteroidId))
        } else {
            mapperNet.map(getFromNet(asteroidId))
        }
    }

    suspend fun checkAsteroidInDb(asteroidId: Int): Boolean {
        var isExists = false
        val result = CoroutineScope(Dispatchers.Main).async {
            val asteroidFromDb = databaseDao.getDetails(asteroidId)
            // asteroidFromDb isn't always true
            if (asteroidFromDb != null) isExists = true
            return@async isExists
        }
        result.await()
        return isExists
    }

    private suspend fun getFromNet(asteroidId: Int): AsteroidDetailResponse {
        val response = service.getAsteroidDetail(asteroidId.toString())
        val shadowResponse = response.body()
        return shadowResponse ?: throw Exceptions.EmptyResponse
    }

    private suspend fun getFromDb(asteroidId: Int): AsteroidWithVisits {
        return databaseDao.getDetails(asteroidId)
    }
}