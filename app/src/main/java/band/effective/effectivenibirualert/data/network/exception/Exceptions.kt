package band.effective.effectivenibirualert.data.network.exception

import java.io.IOException

sealed class Exceptions {

    object NoInternet: IOException("No internet connection")
    object UnknownServer: IOException("An unknown server exception occurred")
    object InternalServerError: IOException("An internal server error occurred")
    object UnprocessableEntity: IOException("422 exception occurred, could not identify it")
    object NotFound: IOException("404 exception occurred, could not identify it")
    object EmptyResponse: IOException("Empty response")
    data class BadRequest(val error: String) : IOException(error)
}