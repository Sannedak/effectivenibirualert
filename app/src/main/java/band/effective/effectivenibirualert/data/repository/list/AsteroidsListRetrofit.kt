package band.effective.effectivenibirualert.data.repository.list

import band.effective.effectivenibirualert.data.model.list.AsteroidsListResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface AsteroidsListRetrofit {
    @GET("/neo/rest/v1/feed")
    suspend fun getAsteroidsList(
        @Query("start_date") startDate: String,
        @Query("end_date") endDate: String
    ): Response<AsteroidsListResponse>
}