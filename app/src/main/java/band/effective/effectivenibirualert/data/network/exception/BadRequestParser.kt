package band.effective.effectivenibirualert.data.network.exception

import com.squareup.moshi.*
import java.io.IOException

class BadRequestParser {

    private val moshi by lazy { Moshi.Builder().build() }
    private val badRequestAdapter by lazy { moshi.adapter(BadRequest::class.java) }

    fun parse(response: String): IOException {
        val parsedJson = badRequestAdapter.fromJson(response)
        val errorMessage = parsedJson?.errorMessage.toString()
        return Exceptions.BadRequest(errorMessage)
    }
}

data class BadRequest (
    @field:Json(name = "error_message") val errorMessage: String
)