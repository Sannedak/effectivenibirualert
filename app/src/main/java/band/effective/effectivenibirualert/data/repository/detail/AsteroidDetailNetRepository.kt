package band.effective.effectivenibirualert.data.repository.detail

import band.effective.effectivenibirualert.presentation.fragments.detail.model.UiAsteroidDetail

interface AsteroidDetailNetRepository {

    suspend fun getAsteroidDetail(asteroidId: Int): UiAsteroidDetail
}
