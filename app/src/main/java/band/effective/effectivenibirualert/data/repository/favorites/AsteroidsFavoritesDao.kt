package band.effective.effectivenibirualert.data.repository.favorites

import androidx.room.*
import band.effective.effectivenibirualert.data.model.favorites.Asteroid
import band.effective.effectivenibirualert.data.model.favorites.AsteroidWithVisits
import band.effective.effectivenibirualert.data.model.favorites.Visit

@Dao
interface AsteroidsFavoritesDao {
    @Query("SELECT * FROM asteroid")
    suspend fun getList(): List<Asteroid>

    @Transaction
    @Query("SELECT * FROM asteroid WHERE id=:id")
    suspend fun getDetails(id: Int): AsteroidWithVisits

    @Query("DELETE FROM asteroid WHERE id=:id")
    suspend fun deleteAsteroid(id: Int)

    @Query("DELETE FROM visits WHERE asteroid_id=:id")
    suspend fun deleteVisits(id: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAsteroid(asteroid: Asteroid)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertVisit(visit: Visit)
}