package band.effective.effectivenibirualert.data.repository.list

import band.effective.effectivenibirualert.data.model.list.AsteroidsListResponse
import java.util.*

interface AsteroidsListNetRepository {

    suspend fun getAsteroidsList(startDate: Date, endDate: Date): AsteroidsListResponse
}