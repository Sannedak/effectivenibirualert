package band.effective.effectivenibirualert.data.repository.favorites

import androidx.room.Database
import androidx.room.RoomDatabase
import band.effective.effectivenibirualert.data.model.favorites.Asteroid
import band.effective.effectivenibirualert.data.model.favorites.Visit

@Database(entities = [Asteroid::class, Visit::class], version = 1)
abstract class AsteroidsFavoritesDatabase :RoomDatabase() {
    abstract fun favoritesListDao() : AsteroidsFavoritesDao
}