package band.effective.effectivenibirualert.data.model.detail

import band.effective.effectivenibirualert.data.model.list.DiameterResponse
import band.effective.effectivenibirualert.data.model.list.MissDistanceResponse
import band.effective.effectivenibirualert.data.model.list.RelativeVelocityResponse
import com.squareup.moshi.Json

data class AsteroidDetailResponse(
    @field:Json(name = "is_potentially_hazardous_asteroid")
    val isPotentiallyHazardous: Boolean,
    @field:Json(name = "estimated_diameter")
    val estimatedDiameter: DiameterResponse,
    @field:Json(name = "orbital_data")
    val orbitData: OrbitResponse?,
    @field:Json(name = "close_approach_data")
    val ApproachData: List<ApproachData>
)

data class ApproachData(
    @field:Json(name = "close_approach_date")
    val arrivalDate: String,
    @field:Json(name = "close_approach_date_full")
    val arrivalDateFull: String?,
    @field:Json(name = "relative_velocity")
    val relativeVelocity: RelativeVelocityResponse?,
    @field:Json(name = "miss_distance")
    val missDistance: MissDistanceResponse?,
    @field:Json(name = "orbiting_body")
    val orbitingBody: String
)

data class OrbitResponse(
    @field:Json(name = "orbit_class")
    val orbitClass: OrbitClassResponse?
)

data class OrbitClassResponse(
    @field:Json(name = "orbit_class_type")
    val classType: String,
    @field:Json(name = "orbit_class_description")
    val classDescription: String
)
