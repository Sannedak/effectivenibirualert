package band.effective.effectivenibirualert.presentation.fragments.detail.model.database

import band.effective.effectivenibirualert.data.model.favorites.AsteroidWithVisits
import band.effective.effectivenibirualert.presentation.fragments.detail.model.UiAsteroidDetail

class UiAsteroidDetailMapperDb {

    private val mapVisits by lazy { UiAsteroidVisitsMapperDb() }

    fun map(response: AsteroidWithVisits) = with(response) {
        UiAsteroidDetail(
            asteroid.isPotentiallyHazardous,
            asteroid.diameter,
            asteroid.classType,
            asteroid.classDescription,
            mapVisits.map(visits)
        )
    }
}