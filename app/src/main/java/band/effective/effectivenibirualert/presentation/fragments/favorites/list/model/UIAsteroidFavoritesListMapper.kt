package band.effective.effectivenibirualert.presentation.fragments.favorites.list.model

import band.effective.effectivenibirualert.data.model.favorites.Asteroid
import band.effective.effectivenibirualert.presentation.fragments.list.model.UiAsteroidList

class UIAsteroidFavoritesListMapper {

    fun map(list: List<Asteroid>): List<UiAsteroidList> {
        return list.sortedWith(compareBy{it.approachDate})
            .map { mapAsteroid(it) }
    }

    private fun mapAsteroid(asteroid: Asteroid) = with(asteroid) {
        UiAsteroidList(
            id,
            name,
            diameter,
            isPotentiallyHazardous,
            approachDate,
            relativeVelocity,
            missDistance,
            imageSizePx
        )
    }
}