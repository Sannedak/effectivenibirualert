package band.effective.effectivenibirualert.presentation.fragments.detail.model

import android.content.res.Resources
import band.effective.effectivenibirualert.R
import band.effective.effectivenibirualert.data.model.detail.AsteroidDetailResponse
import band.effective.effectivenibirualert.data.model.list.DiameterResponse
import kotlin.math.roundToInt

class UiAsteroidDetailMapper(private val resources: Resources) {

    private val mapVisits by lazy { UiAsteroidVisitsMapper(resources) }

    fun map(response: AsteroidDetailResponse) = with(response) {
        UiAsteroidDetail(
            isPotentiallyHazardous,
            mapDiameter(estimatedDiameter),
            orbitData?.orbitClass?.classType,
            orbitData?.orbitClass?.classDescription,
            mapVisits.map(ApproachData)
        )
    }

    private fun mapDiameter(diameter: DiameterResponse): String {
        val isSmallDiameter = diameter.meters!!.min < 1000
        return if (isSmallDiameter) mapDiameterMeter(diameter) else mapDiameterKilometer(diameter)
    }

    private fun mapDiameterMeter(diameter: DiameterResponse): String {
        val postfix = resources.getString(R.string.unit_meter)
        return resources.getString(
            R.string.textView_diameter,
            diameter.meters?.min?.roundToInt(),
            diameter.meters?.max?.roundToInt(),
            postfix
        )
    }

    private fun mapDiameterKilometer(diameter: DiameterResponse): String {
        val postfix = resources.getString(R.string.unit_kilometer)
        return resources.getString(
            R.string.textView_diameter,
            diameter.kilometers?.min?.roundToInt(),
            diameter.kilometers?.max?.roundToInt(),
            postfix
        )
    }
}