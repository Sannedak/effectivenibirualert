package band.effective.effectivenibirualert.presentation.fragments.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import band.effective.effectivenibirualert.data.model.favorites.Asteroid
import band.effective.effectivenibirualert.data.model.favorites.Visit
import band.effective.effectivenibirualert.data.repository.detail.AsteroidDetailRepository
import band.effective.effectivenibirualert.data.repository.favorites.AsteroidsFavoritesDao
import band.effective.effectivenibirualert.presentation.UiState
import band.effective.effectivenibirualert.presentation.base.exception.createErrorHandler
import band.effective.effectivenibirualert.presentation.fragments.detail.model.UiAsteroidDetail
import kotlinx.coroutines.*

class AsteroidDetailViewModel(
    private val repository: AsteroidDetailRepository,
    private val databaseDao: AsteroidsFavoritesDao,
    private val asteroidListParameters: AsteroidDetailParams
) : ViewModel() {

    private var _viewState = MutableLiveData<UiState<Any>>()
    private var _asteroidStateInDb = MutableLiveData<Boolean>()
    private var _asteroid = MutableLiveData<UiAsteroidDetail>()

    val asteroidStateInDb: LiveData<Boolean>
        get() = _asteroidStateInDb

    init {
        loadAsteroidsWithProgress()
        checkAsteroidInDb(asteroidListParameters.id)
    }

    val viewState: LiveData<UiState<Any>>
        get() = _viewState

    fun onRefresh() {
        val handler = createErrorHandler {
            _viewState.value = UiState.ErrorDialog(it)
        }
        loadAsteroidInformation(handler)
    }

    fun deleteAsteroid() {
        viewModelScope.launch {
            databaseDao.deleteAsteroid(asteroidListParameters.id)
            databaseDao.deleteVisits(asteroidListParameters.id)
        }
    }

    fun saveAsteroid() {
        val asteroidParams = _asteroid.value!!
        val asteroid = Asteroid(
            asteroidListParameters.id,
            asteroidListParameters.name,
            asteroidParams.diameter,
            asteroidParams.isPotentiallyHazardous,
            asteroidListParameters.approachDate,
            asteroidListParameters.relativeVelocity,
            asteroidListParameters.missDistance,
            asteroidListParameters.imageSize,
            asteroidParams.orbitClassType,
            asteroidParams.orbitClassDescription
        )
        viewModelScope.launch {
            databaseDao.insertAsteroid(asteroid)
            asteroidParams.visits.forEach {
                val visit = Visit(
                    0,
                    it.id,
                    asteroidListParameters.id,
                    it.approachDate,
                    it.relativeVelocity,
                    it.missDistance,
                    it.orbitPlanet
                )
                databaseDao.insertVisit(visit)
            }
        }
    }

    private fun checkAsteroidInDb(asteroidId: Int) {
        _asteroidStateInDb.value = false
        viewModelScope.launch {
            val asteroidFromDb = repository.checkAsteroidInDb(asteroidId)
            if (asteroidFromDb) _asteroidStateInDb.value = true
        }
    }

    private fun loadAsteroidsWithProgress() {
        val handler = createErrorHandler {
            _viewState.value = UiState.Error(it, ::loadAsteroidsWithProgress)
        }
        loadAsteroidInformation(handler)
    }

    private fun loadAsteroidInformation(handler: CoroutineExceptionHandler) {
        _viewState.value = UiState.Loading
        viewModelScope.launch(handler) {
            val asteroid = repository.getAsteroidDetail(asteroidListParameters.id)
            _asteroid.value = asteroid
            _viewState.value = UiState.DetailContent(asteroid)
        }
    }
}