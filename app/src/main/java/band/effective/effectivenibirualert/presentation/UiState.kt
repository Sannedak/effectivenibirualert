package band.effective.effectivenibirualert.presentation

import band.effective.effectivenibirualert.presentation.base.exception.ExceptionInfo
import band.effective.effectivenibirualert.presentation.fragments.detail.model.UiAsteroidDetail
import band.effective.effectivenibirualert.presentation.fragments.list.model.UiAsteroidList

sealed class UiState<T> {

    object Loading : UiState<Any>()
    data class ListContent<T>(val data: List<UiAsteroidList>) : UiState<T>()
    data class DetailContent<T>(val data: UiAsteroidDetail) : UiState<T>()
    data class Error(val error: ExceptionInfo, val retryCallback: () -> Unit) : UiState<Any>()
    data class ErrorDialog(val error: ExceptionInfo) : UiState<Any>()
}