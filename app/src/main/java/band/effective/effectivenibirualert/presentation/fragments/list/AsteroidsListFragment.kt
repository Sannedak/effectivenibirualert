package band.effective.effectivenibirualert.presentation.fragments.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import band.effective.effectivenibirualert.R
import band.effective.effectivenibirualert.databinding.FragmentAsteroidsListBinding
import band.effective.effectivenibirualert.di.NibiruApplication
import band.effective.effectivenibirualert.presentation.UiState
import band.effective.effectivenibirualert.presentation.base.exception.ExceptionInfo
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import band.effective.effectivenibirualert.presentation.fragments.list.model.UiAsteroidList
import band.effective.effectivenibirualert.presentation.setVisible
import java.util.*
import band.effective.effectivenibirualert.presentation.MainActivity
import band.effective.effectivenibirualert.presentation.fragments.detail.AsteroidDetailParams
import org.koin.androidx.viewmodel.ext.android.viewModel

class AsteroidsListFragment : Fragment() {

    private val asteroidsListAdapter by lazy { AsteroidsListAdapter(::showAsteroidDetail) }
    private val dateFormatter by lazy { NibiruApplication().uiDatePattern }
    private val datePickerBuilder by lazy { MaterialDatePicker.Builder.dateRangePicker() }
    private val datePicker by lazy { datePickerBuilder.build() }

    private var _binding: FragmentAsteroidsListBinding? = null

    private val binding
        get() = _binding!!

    private val viewModel:AsteroidsListViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAsteroidsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecycler()
        initEventListeners()
    }

    private fun observeViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is UiState.Loading -> changeLoadingState(true)
                    is UiState.ListContent -> {
                        updateList(it.data)
                        changeLoadingState(false)
                    }
                    is UiState.ErrorDialog -> showErrorDialog(it.error)
                    is UiState.Error -> {
                        showError(it.error, it.retryCallback)
                    }
                }
            }
        )
        viewModel.datePair.observe(
            viewLifecycleOwner,
            Observer {
                setTextOnButtons(it.first!!, it.second!!)
            }
        )
    }

    private fun setUpRecycler() {
        binding.asteroidsList.adapter = asteroidsListAdapter
    }

    private fun initEventListeners() {
        binding.buttonSetDates.setOnClickListener { showDateRangePicker() }
        binding.refreshContainer.setOnRefreshListener { viewModel.onRefresh() }
    }

    private fun showAsteroidDetail(asteroid: AsteroidDetailParams) {
        val bundle = bundleOf("asteroid" to asteroid)
        Navigation.findNavController(requireView()).navigate(R.id.action_asteroidsListFragment_to_asteroidDetailFragment, bundle)
        val mainActivity = activity as MainActivity
        mainActivity.changeBottomNavVisibility(false)
    }

    private fun showDateRangePicker() {
        val datePair = viewModel.datePair.value
        datePickerBuilder.setSelection(datePair)
        datePicker.show(childFragmentManager, datePicker.toString())
        datePicker.addOnPositiveButtonClickListener {
            viewModel.updateDates(it.first!!, it.second!!)
            viewModel.loadAsteroidsWithProgress()
        }
    }

    private fun updateList(response: List<UiAsteroidList>) {
        binding.errorGroup.setVisible(false)
        binding.asteroidsList.setVisible(true)
        changeLoadingState(false)
        val itemCount = response.size
        asteroidsListAdapter.clearAndFillList(response)
        binding.asteroidsList.adapter?.notifyDataSetChanged()
        binding.textViewPeriod.text =
            resources.getQuantityString(R.plurals.textView_things_plural, itemCount, itemCount)
    }

    private fun changeLoadingState(isRefreshing: Boolean) {
        binding.refreshContainer.isRefreshing = isRefreshing
    }

    private fun showError(error: ExceptionInfo, retryCallback: () -> Unit) {
        binding.asteroidsList.setVisible(false)
        binding.errorGroup.setVisible(true)
        binding.errorContainer.buttonUpdate.setOnClickListener { retryCallback() }
        binding.errorContainer.textViewError.text = error.messageRes
        changeLoadingState(false)
    }

    private fun showErrorDialog(error: ExceptionInfo) {
        val builder = MaterialAlertDialogBuilder(requireContext())
        builder
            .setMessage(error.messageRes)
            .setCancelable(false)
            .setNegativeButton(
                "OK"
            ) { dialog, _ -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
        changeLoadingState(false)
    }

    private fun setTextOnButtons(firstDate: Long, secondDate: Long) {
        val firstDateText = dateFormatter.format(Date(firstDate))
        val secondDateText = dateFormatter.format(Date(secondDate))
        val buttonText = "$firstDateText - $secondDateText"
        binding.buttonSetDates.text = buttonText
    }
}