package band.effective.effectivenibirualert.presentation.fragments.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import band.effective.effectivenibirualert.R
import band.effective.effectivenibirualert.presentation.fragments.detail.AsteroidDetailParams
import band.effective.effectivenibirualert.presentation.fragments.list.model.UiAsteroidList
import kotlinx.android.synthetic.main.asteroids_list_item.view.*

class AsteroidsListAdapter(private val showAsteroidDetail: (asteroid: AsteroidDetailParams) -> Unit) :
    RecyclerView.Adapter<AsteroidHolder>() {

    init {
        setHasStableIds(true)
    }

    private val asteroids = mutableListOf<UiAsteroidList>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AsteroidHolder {
        return AsteroidHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.asteroids_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemId(position: Int): Long {
        val positionInRecycler = asteroids[position].id
        return positionInRecycler.toLong()
    }

    override fun getItemCount() = asteroids.size

    override fun onBindViewHolder(holder: AsteroidHolder, position: Int) {
        holder.bind(asteroids[position], holder.itemView, showAsteroidDetail)
    }

    fun clearAndFillList(response: List<UiAsteroidList>) {
        asteroids.clear()
        asteroids.addAll(response)
    }
}

class AsteroidHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(
        asteroid: UiAsteroidList,
        view: View,
        showAsteroidDetail: (asteroidInformation: AsteroidDetailParams) -> Unit
    ) {
        val asteroidInformation =
            AsteroidDetailParams(
                asteroid.id,
                asteroid.name,
                asteroid.approachDate,
                asteroid.relativeVelocity,
                asteroid.missDistance,
                asteroid.imageSizePx
            )
        view.setOnClickListener { showAsteroidDetail(asteroidInformation) }
        view.asteroid_name.text = asteroid.name
        view.asteroid_hazardous.visibility =
            if (asteroid.isPotentiallyHazardous) View.VISIBLE else View.GONE
        view.asteroid_diameter.text = asteroid.diameter
        view.asteroid_approach_date.text = asteroid.approachDate
        view.asteroid_relative_velocity.text = asteroid.relativeVelocity
        view.asteroid_miss_distance.text = asteroid.missDistance
        view.asteroid_image.layoutParams.height = asteroid.imageSizePx
        view.asteroid_image.layoutParams.width = asteroid.imageSizePx
    }
}