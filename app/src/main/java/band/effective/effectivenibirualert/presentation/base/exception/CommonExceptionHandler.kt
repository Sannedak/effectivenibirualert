package band.effective.effectivenibirualert.presentation.base.exception

import kotlinx.coroutines.CoroutineExceptionHandler

class CommonExceptionHandler(private val onError: ((ExceptionInfo) -> Unit)? = null) {

    private val exceptionInfoMapper by lazy { ExceptionInfoMapper() }

    val coroutineHandler = CoroutineExceptionHandler { _, throwable ->
        throwable.printStackTrace()
        onError?.let {
            val exceptionInfo = exceptionInfoMapper.map(throwable)
            it.invoke(exceptionInfo)
        }
    }
}

fun createErrorHandler(onError: (ExceptionInfo) -> Unit) =
    CommonExceptionHandler(onError).coroutineHandler