package band.effective.effectivenibirualert.presentation.fragments.list.model

data class UiAsteroidList (
    val id: Int,
    val name: String,
    val diameter: String,
    val isPotentiallyHazardous: Boolean,
    val approachDate: String,
    val relativeVelocity: String,
    val missDistance: String,
    val imageSizePx: Int
)