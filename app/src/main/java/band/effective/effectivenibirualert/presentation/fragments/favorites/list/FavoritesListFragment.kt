package band.effective.effectivenibirualert.presentation.fragments.favorites.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import band.effective.effectivenibirualert.R
import band.effective.effectivenibirualert.databinding.FragmentAsteroidsFavoritesListBinding
import band.effective.effectivenibirualert.presentation.MainActivity
import band.effective.effectivenibirualert.presentation.UiState
import band.effective.effectivenibirualert.presentation.fragments.detail.AsteroidDetailParams
import band.effective.effectivenibirualert.presentation.fragments.list.AsteroidsListAdapter
import band.effective.effectivenibirualert.presentation.fragments.list.model.UiAsteroidList
import org.koin.android.ext.android.inject

class FavoritesListFragment : Fragment() {

    private val asteroidsFavoritesAdapter by lazy { AsteroidsListAdapter(::showAsteroidDetail) }

    private val viewModel: FavoritesListViewModel by inject()

    private var _binding: FragmentAsteroidsFavoritesListBinding? = null

    private val binding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAsteroidsFavoritesListBinding.inflate(inflater, container, false)
        return  binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is UiState.ListContent -> {
                        updateList(it.data)
                    }
                }
            }
        )
    }

    private fun updateList(data: List<UiAsteroidList>) {
        asteroidsFavoritesAdapter.clearAndFillList(data)
        binding.asteroidsList.adapter?.notifyDataSetChanged()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecycler()

    }

    private fun setUpRecycler() {
        binding.asteroidsList.adapter = asteroidsFavoritesAdapter
    }

    private fun showAsteroidDetail(asteroid: AsteroidDetailParams) {
        val bundle = bundleOf("asteroid" to asteroid)
        Navigation.findNavController(requireView()).navigate(R.id.action_favoritesListFragment_to_asteroidDetailFragment, bundle)
        val mainActivity = activity as MainActivity
        mainActivity.changeBottomNavVisibility(false)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}