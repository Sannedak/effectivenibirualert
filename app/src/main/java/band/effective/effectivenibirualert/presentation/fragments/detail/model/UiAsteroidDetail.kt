package band.effective.effectivenibirualert.presentation.fragments.detail.model

data class UiAsteroidDetail (
    val isPotentiallyHazardous: Boolean,
    val diameter: String,
    val orbitClassType: String?,
    val orbitClassDescription: String?,
    val visits: List<VisitsDetail>
)

data class VisitsDetail(
    val id: Int,
    val approachDate: String,
    val relativeVelocity: String,
    val missDistance: String,
    val orbitPlanet: String
)
