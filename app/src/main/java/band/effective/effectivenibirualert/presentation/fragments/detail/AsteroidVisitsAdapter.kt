package band.effective.effectivenibirualert.presentation.fragments.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import band.effective.effectivenibirualert.R
import band.effective.effectivenibirualert.presentation.fragments.detail.model.VisitsDetail
import kotlinx.android.synthetic.main.asteroid_visit_item.view.*

class AsteroidVisitsAdapter : RecyclerView.Adapter<AsteroidVisitHolder>() {

    init {
        setHasStableIds(true)
    }

    private val visits = mutableListOf<VisitsDetail>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AsteroidVisitHolder {
        return AsteroidVisitHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.asteroid_visit_item,
                parent,
                false
            )
        )
    }

    override fun getItemId(position: Int): Long {
        val positionInRecycler = visits[position].id
        return positionInRecycler.toLong()
    }

    override fun getItemCount() = visits.size

    override fun onBindViewHolder(holder: AsteroidVisitHolder, position: Int) {
        holder.bind(visits[position], holder.itemView)
    }

    fun clearAndFillList(response: List<VisitsDetail>) {
        visits.clear()
        visits.addAll(response)
    }
}

class AsteroidVisitHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(visit: VisitsDetail, view: View) {
        view.date_time.text = visit.approachDate
        view.relative_velocity.text = visit.relativeVelocity
        view.miss_distance.text = visit.missDistance
        view.orbit.text = visit.orbitPlanet
    }
}