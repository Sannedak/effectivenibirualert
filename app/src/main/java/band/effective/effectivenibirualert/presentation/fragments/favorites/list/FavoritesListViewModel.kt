package band.effective.effectivenibirualert.presentation.fragments.favorites.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import band.effective.effectivenibirualert.data.repository.favorites.AsteroidsFavoritesDao
import band.effective.effectivenibirualert.presentation.UiState
import band.effective.effectivenibirualert.presentation.fragments.favorites.list.model.UIAsteroidFavoritesListMapper
import kotlinx.coroutines.*

class FavoritesListViewModel(
    private val mapper: UIAsteroidFavoritesListMapper,
    private val databaseDao: AsteroidsFavoritesDao
) : ViewModel() {

    private var _viewState = MutableLiveData<UiState<Any>>()

    val viewState: LiveData<UiState<Any>>
        get() = _viewState

    init {
        loadAsteroids()
    }

    private fun loadAsteroids() {
        _viewState.value = UiState.Loading
        viewModelScope.launch {
            val asteroids = databaseDao.getList()
            val asteroidsList = mapper.map(asteroids)
            _viewState.value = UiState.ListContent(asteroidsList)
        }
    }
}