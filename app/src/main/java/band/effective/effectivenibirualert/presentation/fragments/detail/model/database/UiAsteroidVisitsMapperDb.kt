package band.effective.effectivenibirualert.presentation.fragments.detail.model.database

import band.effective.effectivenibirualert.data.model.favorites.Visit
import band.effective.effectivenibirualert.presentation.fragments.detail.model.VisitsDetail

class UiAsteroidVisitsMapperDb {

    fun map(visits: List<Visit>): List<VisitsDetail> = visits.map { mapVisits(it) }

    private fun mapVisits(response: Visit): VisitsDetail = with(response) {
        VisitsDetail(
            id,
            approachDate,
            relativeVelocity,
            missDistance,
            orbitingBody
        )
    }
}