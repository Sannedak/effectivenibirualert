package band.effective.effectivenibirualert.presentation.fragments.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import band.effective.effectivenibirualert.presentation.UiState
import band.effective.effectivenibirualert.presentation.base.exception.createErrorHandler
import band.effective.effectivenibirualert.presentation.fragments.list.model.UiAsteroidListMapper
import kotlinx.coroutines.*
import java.util.*
import androidx.core.util.Pair
import band.effective.effectivenibirualert.data.repository.list.AsteroidsListNasaNetRepository

class AsteroidsListViewModel(
    private val mapper: UiAsteroidListMapper,
    private val repository: AsteroidsListNasaNetRepository
) : ViewModel() {

    private val calendar by lazy { Calendar.getInstance() }

    private var _viewState = MutableLiveData<UiState<Any>>()
    private var _datePair = MutableLiveData<Pair<Long, Long>>()

    init {
        loadAsteroidsWithInitialDate()
    }

    val viewState: LiveData<UiState<Any>>
        get() = _viewState

    val datePair: LiveData<Pair<Long, Long>>
        get() = _datePair

    fun updateDates(firstDate: Long, secondDate: Long) {
        _datePair.value = Pair(firstDate, secondDate)
    }

    fun onRefresh() {
        val handler = createErrorHandler {
            _viewState.value = UiState.ErrorDialog(it)
        }
        loadAsteroids(handler)
    }

    fun loadAsteroidsWithProgress() {
        val handler = createErrorHandler {
            _viewState.value = UiState.Error(it, ::loadAsteroidsWithProgress)
        }
        loadAsteroids(handler)
    }

    private fun loadAsteroidsWithInitialDate() {
        val startDate = calendar.time
        calendar.add(Calendar.DAY_OF_MONTH, 6)
        val endDate = calendar.time
        _datePair.value = Pair(startDate.time, endDate.time)
        loadAsteroidsWithProgress()
    }

    private fun loadAsteroids(handler: CoroutineExceptionHandler) {
        _viewState.value = UiState.Loading
        val startDate = _datePair.value?.first?.let { Date(it) } ?: calendar.time
        val endDate = _datePair.value?.second?.let { Date(it) } ?: calendar.time
        CoroutineScope(Dispatchers.Main).launch(handler) {
            val response = withContext(Dispatchers.IO) {
                val asteroidResponseMap = repository.getAsteroidsList(startDate, endDate).days
                asteroidResponseMap.flatMap { (_, list) -> list }
            }
            val asteroidsList = mapper.map(response)
            _viewState.value = UiState.ListContent(asteroidsList)
        }
    }
}