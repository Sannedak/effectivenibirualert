package band.effective.effectivenibirualert.presentation.base.exception

import band.effective.effectivenibirualert.data.network.exception.Exceptions


internal class ExceptionInfoMapper {

    fun map(throwable: Throwable) = ExceptionInfo(
        mapErrorMessage(throwable)
    )

    private fun mapErrorMessage(exception: Throwable) = when (exception) {
        is Exceptions.NoInternet -> exception.localizedMessage
        is Exceptions.NotFound -> exception.localizedMessage
        is Exceptions.BadRequest -> exception.error
        else -> exception.localizedMessage
    }
}
