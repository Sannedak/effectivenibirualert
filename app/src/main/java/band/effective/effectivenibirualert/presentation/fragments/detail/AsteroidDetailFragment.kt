package band.effective.effectivenibirualert.presentation.fragments.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import band.effective.effectivenibirualert.databinding.FragmentAsteroidDetailBinding
import band.effective.effectivenibirualert.presentation.MainActivity
import band.effective.effectivenibirualert.presentation.UiState
import band.effective.effectivenibirualert.presentation.base.exception.ExceptionInfo
import band.effective.effectivenibirualert.presentation.fragments.detail.model.UiAsteroidDetail
import band.effective.effectivenibirualert.presentation.setVisible
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class AsteroidDetailFragment : Fragment() {

    private val asteroidVisitsAdapter by lazy { AsteroidVisitsAdapter() }

    private lateinit var detailParams: AsteroidDetailParams

    private var _binding: FragmentAsteroidDetailBinding? = null

    private val binding
        get() = _binding!!

    private val viewModel: AsteroidDetailViewModel by viewModel { parametersOf(detailParams) }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAsteroidDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detailParams = getArgumentsParams()
        changeInitialParamsOnScreen()
        setUpRecycler()
        initEventListeners()
    }

    private fun initEventListeners() {
        binding.toolbar.setNavigationOnClickListener { backToPreviousScreen() }
        binding.refreshContainer.setOnRefreshListener { viewModel.onRefresh() }
        requireActivity().onBackPressedDispatcher.addCallback { backToPreviousScreen() }
    }

    private fun backToPreviousScreen() {
        findNavController().popBackStack()
        val mainActivity = activity as MainActivity
        mainActivity.changeBottomNavVisibility(true)
    }

    private fun changeInitialParamsOnScreen() {
        binding.buttonFavorite.setOnClickListener {
            val buttonState = binding.buttonFavorite.isSelected
            if (!buttonState) {
                viewModel.saveAsteroid()
                binding.buttonFavorite.isSelected = true
                Toast.makeText(context, "Saved", Toast.LENGTH_LONG).show()
            } else {
                viewModel.deleteAsteroid()
                binding.buttonFavorite.isSelected = false
                Toast.makeText(context, "Deleted", Toast.LENGTH_LONG).show()
            }
        }
        binding.toolbar.title = detailParams.name
        binding.asteroidImage.layoutParams.height = detailParams.imageSize
        binding.asteroidImage.layoutParams.width = detailParams.imageSize
    }

    private fun setUpRecycler() {
        binding.visits.adapter = asteroidVisitsAdapter
    }

    private fun observeViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is UiState.Loading -> changeLoadingState(true)
                    is UiState.DetailContent -> {
                        updateInformation(it.data)
                        changeLoadingState(false)
                    }
                    is UiState.ErrorDialog -> showErrorDialog(it.error)
                    is UiState.Error -> showError(it.error, it.retryCallback)
                }
            }
        )
        viewModel.asteroidStateInDb.observe(
            viewLifecycleOwner,
            Observer { binding.buttonFavorite.isSelected = it })
    }

    private fun showError(error: ExceptionInfo, retryCallback: () -> Unit) {
        binding.contentGroup.setVisible(false)
        binding.classDescription.setVisible(false)
        binding.errorGroup.setVisible(true)
        binding.errorContainer.buttonUpdate.setOnClickListener { retryCallback() }
        binding.errorContainer.textViewError.text = error.messageRes
        changeLoadingState(false)
    }

    private fun updateInformation(data: UiAsteroidDetail) {
        binding.contentGroup.setVisible(false)
        binding.contentGroup.setVisible(true)
        binding.classDescription.setVisible(true)
        changeLoadingState(false)
        binding.hazard.setVisible(data.isPotentiallyHazardous)
        binding.diameter.text = data.diameter
        if (data.orbitClassType == null) {
            binding.classDescription.setVisible(false)
        } else {
            binding.asteroidClass.text = data.orbitClassType
            binding.classDescription.text = data.orbitClassDescription
        }
        asteroidVisitsAdapter.clearAndFillList(data.visits)
        binding.visits.adapter?.notifyDataSetChanged()
    }

    private fun changeLoadingState(isRefreshing: Boolean) {
        binding.refreshContainer.isRefreshing = isRefreshing
    }

    private fun showErrorDialog(error: ExceptionInfo) {
        val builder = MaterialAlertDialogBuilder(requireContext())
        builder
            .setMessage(error.messageRes)
            .setCancelable(false)
            .setNegativeButton(
                "OK"
            ) { dialog, _ -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
        changeLoadingState(false)
    }

    private fun getArgumentsParams(): AsteroidDetailParams {
        return requireArguments().getParcelable<AsteroidDetailParams>("asteroid") as AsteroidDetailParams
    }
}