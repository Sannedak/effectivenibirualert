package band.effective.effectivenibirualert.presentation.fragments.detail

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class AsteroidDetailParams(
    val id: Int,
    val name: String,
    val approachDate: String,
    val relativeVelocity: String,
    val missDistance: String,
    val imageSize: Int
) : Parcelable