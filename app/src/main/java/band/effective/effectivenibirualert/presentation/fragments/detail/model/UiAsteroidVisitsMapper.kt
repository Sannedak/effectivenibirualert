package band.effective.effectivenibirualert.presentation.fragments.detail.model

import android.content.res.Resources
import band.effective.effectivenibirualert.R
import band.effective.effectivenibirualert.data.model.detail.ApproachData
import band.effective.effectivenibirualert.data.model.list.MissDistanceResponse
import band.effective.effectivenibirualert.data.model.list.RelativeVelocityResponse
import band.effective.effectivenibirualert.di.NibiruApplication
import kotlin.math.roundToInt

class UiAsteroidVisitsMapper(private val resources: Resources) {

    private val dateFormatterNet by lazy { NibiruApplication().netDatePattern }
    private val dateFormatterNetFull by lazy { NibiruApplication().netDateFullPattern }
    private val dateFormatterUi by lazy { NibiruApplication().uiDatePattern }
    private val dateFormatterUiFull by lazy { NibiruApplication().uiVisitsDatePattern }

    private var id: Int = 0

    fun map(visits: List<ApproachData>): List<VisitsDetail> = visits.map { mapVisits(it) }

    private fun mapVisits(response: ApproachData): VisitsDetail = with(response) {
        VisitsDetail(
            mapId(),
            mapApproachDate(arrivalDate, arrivalDateFull),
            mapRelativeVelocity(relativeVelocity!!),
            mapMissDistance(missDistance!!),
            mapOrbitingBody(orbitingBody)
        )
    }

    private fun mapId(): Int {
        return id++
    }

    private fun mapApproachDate(arrivalDate: String, arrivalDateFull: String?): String {
        return if (arrivalDateFull == null) {
            val netArrivalDate = dateFormatterNet.parse(arrivalDate)
            dateFormatterUi.format(netArrivalDate!!)
        } else {
            val netArrivalDate = dateFormatterNetFull.parse(arrivalDateFull)
            dateFormatterUiFull.format(netArrivalDate!!)
        }
    }

    private fun mapRelativeVelocity(velocity: RelativeVelocityResponse): String {
        val isSmallVelocity = velocity.kilometersPerHour < 1000
        return if (isSmallVelocity) mapVelocityKmPerHour(velocity) else mapVelocityKmPerSec(velocity)
    }


    private fun mapVelocityKmPerHour(velocity: RelativeVelocityResponse): String {
        val postfix = resources.getString(R.string.unit_km_per_hour)
        return resources.getString(
            R.string.textView_relative_velocity,
            velocity.kilometersPerHour.roundToInt(),
            postfix
        )
    }

    private fun mapVelocityKmPerSec(velocity: RelativeVelocityResponse): String {
        val postfix = resources.getString(R.string.unit_km_per_second)
        return resources.getString(
            R.string.textView_relative_velocity,
            velocity.kilometersPerSecond.roundToInt(),
            postfix
        )
    }

    private fun mapMissDistance(missDistance: MissDistanceResponse): String = when {
        missDistance.miles < 1000 -> mapMissDistanceMiles(missDistance)
        missDistance.kilometers < 1000 -> mapMissDistanceKilometers(missDistance)
        missDistance.lunar < 1000 -> mapMissDistanceLunar(missDistance)
        else -> mapMissDistanceLunarAstronomical(missDistance)
    }

    private fun mapMissDistanceMiles(missDistance: MissDistanceResponse): String {
        val milesPlural = resources.getQuantityString(
            R.plurals.unit_mile_plural,
            missDistance.miles.roundToInt()
        )
        return resources.getString(
            R.string.textView_miss_on,
            missDistance.miles,
            milesPlural
        )
    }

    private fun mapMissDistanceKilometers(missDistance: MissDistanceResponse): String {
        val prefix = resources.getString(R.string.unit_kilometer)
        return resources.getString(
            R.string.textView_miss_on,
            missDistance.kilometers,
            prefix
        )
    }

    private fun mapMissDistanceLunar(missDistance: MissDistanceResponse): String {
        val lunarPlural = resources.getQuantityString(
            R.plurals.unit_lunar_plural,
            missDistance.lunar.roundToInt()
        )
        return resources.getString(
            R.string.textView_miss_on,
            missDistance.lunar.roundToInt(),
            lunarPlural
        )
    }

    private fun mapMissDistanceLunarAstronomical(missDistance: MissDistanceResponse): String {
        val prefix = resources.getString(R.string.unit_astronomic)
        return resources.getString(
            R.string.textView_miss_on,
            missDistance.astronomical,
            prefix
        )
    }

    private fun mapOrbitingBody(orbitingBody: String): String =
        resources.getString(R.string.textView_orbit_around, orbitingBody)
}