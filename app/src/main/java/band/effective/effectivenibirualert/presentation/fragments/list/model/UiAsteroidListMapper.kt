package band.effective.effectivenibirualert.presentation.fragments.list.model

import android.content.res.Resources
import band.effective.effectivenibirualert.R
import band.effective.effectivenibirualert.data.model.list.AsteroidListResponse
import band.effective.effectivenibirualert.data.model.list.DiameterResponse
import band.effective.effectivenibirualert.data.model.list.MissDistanceResponse
import band.effective.effectivenibirualert.data.model.list.RelativeVelocityResponse
import band.effective.effectivenibirualert.di.NibiruApplication
import kotlin.math.roundToInt

class UiAsteroidListMapper(private val resources: Resources) {

    companion object {
        private const val IMAGE_MAX_SIZE_DP = 108
    }

    private val regex by lazy { "[^A-Za-z0-9 ]".toRegex() }
    private val dateFormatterNet by lazy { NibiruApplication().netDatePattern }
    private val dateFormatterUi by lazy { NibiruApplication().uiDatePattern }
    private val asteroidImageMaxSizePx by lazy {
        resources.getDimension(R.dimen.asteroid_image_max_size_dp).roundToInt()
    }
    private val asteroidImageMinSizePx by lazy {
        resources.getDimension(R.dimen.asteroid_image_min_size_dp).roundToInt()
    }

    fun map(list: List<AsteroidListResponse>): List<UiAsteroidList> {
        val averageDiameter = calculateAverageAsteroidsDiameter(list)
        return list.sortedWith(compareBy { dateFormatterNet.parse(it.approachData[0].arrivalDate) })
            .map { mapAsteroid(it, averageDiameter) }
    }

    private fun calculateAverageAsteroidsDiameter(list: List<AsteroidListResponse>) =
        list.sumByDouble { it.calculateAverageDiameter()!! } / list.size

    private fun mapAsteroid(response: AsteroidListResponse, averageDiameter: Double) =
        with(response) {
            UiAsteroidList(
                id,
                mapName(name, isPotentiallyHazardous),
                mapDiameter(estimatedDiameter!!),
                isPotentiallyHazardous,
                mapApproachDate(approachData[0].arrivalDate),
                mapRelativeVelocity(approachData[0].relativeVelocity!!),
                mapMissDistance(approachData[0].missDistance!!),
                mapImageSize(averageDiameter)
            )
        }

    private fun mapDiameter(diameter: DiameterResponse): String {
        return if (diameter.meters!!.min < 1000)
            resources.getString(
                R.string.textView_diameter,
                diameter.meters.min.roundToInt(),
                diameter.meters.max.roundToInt(),
                resources.getString(R.string.unit_meter)
            )
        else
            resources.getString(
                R.string.textView_diameter,
                diameter.kilometers!!.min.roundToInt(),
                diameter.kilometers.max.roundToInt(),
                resources.getString(R.string.unit_kilometer)
            )
    }

    private fun mapName(name: String, isPotentiallyHazardous: Boolean): String {
        return if (isPotentiallyHazardous) {
            resources.getString(R.string.textView_nibiru)
        } else {
            return regex.replace(name, "")
        }
    }

    private fun mapApproachDate(arrivalDate: String): String {
        val netArrivalDate = dateFormatterNet.parse(arrivalDate)
        return resources.getString(
            R.string.textView_approachDate,
            dateFormatterUi.format(netArrivalDate!!)
        )
    }

    private fun mapRelativeVelocity(velocity: RelativeVelocityResponse): String {
        val isSmallVelocity = velocity.kilometersPerHour < 1000
        return if (isSmallVelocity) mapVelocityKmPerHour(velocity) else mapVelocityKmPerSec(velocity)
    }

    private fun mapVelocityKmPerHour(velocity: RelativeVelocityResponse): String {
        val postfix = resources.getString(R.string.unit_km_per_hour)
        return resources.getString(
            R.string.textView_relative_velocity,
            velocity.kilometersPerHour.roundToInt(),
            postfix
        )
    }

    private fun mapVelocityKmPerSec(velocity: RelativeVelocityResponse): String {
        val postfix = resources.getString(R.string.unit_km_per_second)
        return resources.getString(
            R.string.textView_relative_velocity,
            velocity.kilometersPerSecond.roundToInt(),
            postfix
        )
    }

    private fun mapMissDistance(missDistance: MissDistanceResponse): String = when {
        missDistance.miles < 1000 -> mapMissDistanceMiles(missDistance)
        missDistance.kilometers < 1000 -> mapMissDistanceKilometers(missDistance)
        missDistance.lunar < 1000 -> mapMissDistanceLunar(missDistance)
        else -> mapMissDistanceLunarAstronomical(missDistance)
    }

    private fun mapMissDistanceMiles(missDistance: MissDistanceResponse): String {
        val milesPlural = resources.getQuantityString(
            R.plurals.unit_mile_plural,
            missDistance.miles.roundToInt()
        )
        return resources.getString(
            R.string.textView_miss_on,
            missDistance.miles,
            milesPlural
        )
    }

    private fun mapMissDistanceKilometers(missDistance: MissDistanceResponse): String {
        val prefix = resources.getString(R.string.unit_kilometer)
        return resources.getString(
            R.string.textView_miss_on,
            missDistance.kilometers,
            prefix
        )
    }

    private fun mapMissDistanceLunar(missDistance: MissDistanceResponse): String {
        val lunarPlural = resources.getQuantityString(
            R.plurals.unit_lunar_plural,
            missDistance.lunar.roundToInt()
        )
        return resources.getString(
            R.string.textView_miss_on,
            missDistance.lunar.roundToInt(),
            lunarPlural
        )
    }

    private fun mapMissDistanceLunarAstronomical(missDistance: MissDistanceResponse): String {
        val prefix = resources.getString(R.string.unit_astronomic)
        return resources.getString(
            R.string.textView_miss_on,
            missDistance.astronomical,
            prefix
        )
    }

    private fun AsteroidListResponse.mapImageSize(averageDiameter: Double): Int {
        val averageSize = calculateAverageDiameter()!!.roundToInt()
        return calculateImageSizePx(averageSize, averageDiameter)
    }

    private fun calculateImageSizePx(averageSize: Int, averageDiameter: Double): Int {
        val imageSize = ((averageSize / averageDiameter * 2) * IMAGE_MAX_SIZE_DP).roundToInt()
        return when {
            imageSize > asteroidImageMaxSizePx -> asteroidImageMaxSizePx
            imageSize < asteroidImageMinSizePx -> asteroidImageMinSizePx
            else -> imageSize
        }
    }

}