package band.effective.effectivenibirualert.di.modules

import band.effective.effectivenibirualert.data.repository.detail.AsteroidDetailRepository
import band.effective.effectivenibirualert.data.repository.list.AsteroidsListNasaNetRepository
import org.koin.core.module.Module
import org.koin.dsl.module

val RepositoryModule: Module = module {
    factory { AsteroidsListNasaNetRepository(get()) }
    factory { AsteroidDetailRepository(get(), get(), get(), get()) }
}