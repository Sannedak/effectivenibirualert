package band.effective.effectivenibirualert.di.modules

import androidx.room.Room
import band.effective.effectivenibirualert.data.repository.favorites.AsteroidsFavoritesDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

val DataBaseModule: Module = module {
    single { Room.databaseBuilder(androidContext(), AsteroidsFavoritesDatabase::class.java, "favorites").build() }
    single { get<AsteroidsFavoritesDatabase>().favoritesListDao() }
}