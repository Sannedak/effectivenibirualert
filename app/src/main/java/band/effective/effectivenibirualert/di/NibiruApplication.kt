package band.effective.effectivenibirualert.di

import android.app.Application
import band.effective.effectivenibirualert.di.modules.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import java.text.SimpleDateFormat
import java.util.*

class NibiruApplication: Application() {

    private val _netDatePattern by lazy { SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH) }
    private val _netDateFullPattern by lazy { SimpleDateFormat("yyyy-MMM-dd HH:mm", Locale.ENGLISH) }
    private val _uiDatePattern by lazy { SimpleDateFormat("dd.MM.yy", Locale.ENGLISH) }
    private val _uiVisitsDatePatternFull by lazy { SimpleDateFormat("dd.MM.yyyy\nhh:mm", Locale.ENGLISH) }

    val netDatePattern: SimpleDateFormat
        get() = _netDatePattern

    val netDateFullPattern: SimpleDateFormat
        get() = _netDateFullPattern

    val uiDatePattern: SimpleDateFormat
        get() = _uiDatePattern

    val uiVisitsDatePattern: SimpleDateFormat
        get() = _uiVisitsDatePatternFull

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger()
            androidContext(this@NibiruApplication)
            modules(listOf(
                NetModule,
                RepositoryModule,
                MapperModule,
                ViewModelModule,
                DataBaseModule
            ))
        }
    }
}