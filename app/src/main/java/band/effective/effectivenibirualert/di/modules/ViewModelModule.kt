package band.effective.effectivenibirualert.di.modules

import band.effective.effectivenibirualert.presentation.fragments.detail.AsteroidDetailParams
import band.effective.effectivenibirualert.presentation.fragments.detail.AsteroidDetailViewModel
import band.effective.effectivenibirualert.presentation.fragments.favorites.list.FavoritesListViewModel
import band.effective.effectivenibirualert.presentation.fragments.list.AsteroidsListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val ViewModelModule = module {
    viewModel { (id : AsteroidDetailParams) -> AsteroidDetailViewModel(get(), get(), id)}
    viewModel { AsteroidsListViewModel(get(), get()) }
    viewModel { FavoritesListViewModel(get(), get()) }
}