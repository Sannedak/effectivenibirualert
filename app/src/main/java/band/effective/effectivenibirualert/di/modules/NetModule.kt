package band.effective.effectivenibirualert.di.modules

import band.effective.effectivenibirualert.data.network.RetrofitFactory
import band.effective.effectivenibirualert.data.repository.detail.AsteroidDetailRetrofit
import band.effective.effectivenibirualert.data.repository.list.AsteroidsListRetrofit
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit

val NetModule : Module = module {
    single { RetrofitFactory.create(androidApplication()) }
    single { get<Retrofit>().create(AsteroidsListRetrofit::class.java) }
    single { get<Retrofit>().create(AsteroidDetailRetrofit::class.java) }
}