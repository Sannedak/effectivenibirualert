package band.effective.effectivenibirualert.di.modules

import band.effective.effectivenibirualert.presentation.fragments.detail.model.UiAsteroidDetailMapper
import band.effective.effectivenibirualert.presentation.fragments.detail.model.database.UiAsteroidDetailMapperDb
import band.effective.effectivenibirualert.presentation.fragments.favorites.list.model.UIAsteroidFavoritesListMapper
import band.effective.effectivenibirualert.presentation.fragments.list.model.UiAsteroidListMapper
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

val MapperModule: Module = module {
    single { UiAsteroidListMapper(androidContext().resources) }
    single { UiAsteroidDetailMapper(androidContext().resources) }
    single { UIAsteroidFavoritesListMapper()}
    single { UiAsteroidDetailMapperDb() }
}